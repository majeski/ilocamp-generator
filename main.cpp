#include "generator.h"

Test genTest();

int main() {
    Generator gen("asd");

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // korzystamy z tych funkcji zamiast zwyklego rand()
    Generator::random();        // int32
    Generator::random64();      // int64
    Generator::randomDouble();  // [0.0; 1.0]

    // i shuffle, zamiast random_shuffle (gdzie kluczowe jest aby przekazac jako trzeci argument
    // Generator::getGen())
    std::vector<int> v{1, 2, 3};
    std::shuffle(v.begin(), v.end(), Generator::getGen());

    // poniewaz mozemy dla nich ustawic seed ktory zapewnia takie same wyniki na kazdej maszynie
    Generator::setSeed(0);

    // najlepiej robic setSeed przed wygenerowaniem kazdego testu, tzn przed wolaniem jakiegokolwiek
    // Generator::random() dla tego testu - unikniemy w ten sposob zmian testow przez zmiane ich
    // kolejnosci czy dodanie nowych

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // pierwsza (a raczej zerowa grupa) to test przykladowy, trafia domyslnie do ../in i ../out
    // pozostale testy (zarowno in i out) trafiaja do ../test
    // mozna zmienic sciezki:
    gen.outputPath = "../test";
    gen.inputPath = "../test";
    gen.exampleInPath = "../in";
    gen.exampleOutPath = "../out";

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // 1 test w grupie
    gen.beginGroup();
    {
        Generator::setSeed(1);
        gen.addTest(genTest());  // nazwa: asd0.in
    }
    gen.endGroup();

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // 2 testy w grupie
    gen.beginGroup();
    {
        Generator::setSeed(2);
        gen.addTest(genTest());  // nazwa: asd1a.in

        Generator::setSeed(3);
        gen.addTest(genTest());  // nazwa: asd1b.in

        // literki do nazw testow sa dostawiane gdy w grupie jest wiecej niz 1 test
    }
    gen.endGroup();

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // test poza grupa = grupa z pojedynczym testem
    Generator::setSeed(4);
    gen.addTest(genTest());  // nazwa: asd2.in

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // generowanie testow
    gen.generate();
}

Test genTest() {
    // obiekt reprezentujacy jeden plik z testem
    Test test;

    // mozemy dodac dowolnie wiele argumentow
    test.addLine(1);
    test.addLine(2, 3, 4, 5);

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // nawet jesli nie sa tego samego typu
    test.addLine('6', 7, "osiem", 9.9, 10L);

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // lub dodac vector jak we wczesniejszym generatorze
    std::vector<int> vec{11, 12, 13, 14, 15};
    test.addLine(vec);

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // albo bezposrednio w formacie {}
    test.addLine({16, 17, 18, 19, 20});

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // albo polaczyc obie metody
    // tu juz musi byc vector, nie wystarczy {...}
    test.addLine(21, 22, std::vector<int>{23, 24, 25, 26}, 27);

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // albo dodac cos do testu w formacie jak std::cout
    test.raw() << 28 << " [29 30]" << std::endl;

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // UWAGA: addLine dodaje do testu podane argumenty i na koncu stawia \n
    // w przypadku:
    // test.raw() << "abc: ";
    // test.addLine('a', "bbbb", 120);
    // w tescie znajdzie sie linia "abc: a bbbb 120", tzn. addLine nie tworzy nowej linii, a dodaje
    // na koniec testu i dopiero przechodzi do nowej linii

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // domyslnie argumenty przekazane do addLine sa oddzielane spacja, mozna to zmienic
    test.addLine(31, 32);  // 31 32

    test.setSeparator("");
    test.addLine(33, 34);  // 3334

    test.setSeparator("-");
    test.addLine(35, 36);  // 35-36

    ////////////////////////////////////////////////////////////////////////////////////////////////

    int a = Generator::random() % 10;
    int b = Generator::random() % 10;
    test.addLine(a, b);  // 4-3

    // UWAGA: Nie nalezy w jednym wyrazeniu miec kilku zaleznych od siebie wywolan, a random() jest
    // zalezny od innego wywolania random(), tzn. np. przy wywolaniu funkcji
    // test.addLine(Generator::random(), Generator::random()) nie wiemy w jakiej kolejnosci wykonaja
    // sie funkcje random, czy od lewej czy od prawej strony.
    //
    // W przykladzie wyzej bez pomocniczych zmiennych otrzymalibysmy inne wyniki zaleznie od
    // kompilatora, tzn 4-3 lub 3-4

    return test;
}